# Container Usage
Container commands.
This is a fork of the repo [https://github.com/aschmelyun/docker-compose-laravel]

Visit http://localhost:9999 to see the project page

## Build the containers
add the build flad to build the containers for the first time
```
docker-compose up -d --build
```

otherwise 

```
docker-compose up -d
```


## Check logs:

```
docker-compose logs -f
```


## Execute composer/npm/artisan commands:

```
docker-compose run --rm composer update
```
## NPM commands:
```
docker-compose run --rm npm install
docker-compose run --rm npm run dev
docker-compose run --rm npm run watch
docker-compose run --rm npm run prod
```

## Artisan commands:
```
docker-compose run --rm artisan migrate

```
## Generate the Key

    ```
    docker-compose run --rm artisan key:generate

    ```

## Scaffold Laravel ui

```
docker-compose run --rm composer require laravel/ui

```

### Install front end scaffolding:
```
// Generate basic scaffolding...
docker-compose run --rm artisan ui bootstrap
docker-compose run --rm artisan ui vue
docker-compose run --rm artisan ui react

// Generate login / registration scaffolding...
docker-compose run --rm artisan ui bootstrap --auth
docker-compose run --rm artisan ui vue --auth
docker-compose run --rm artisan ui react --auth

```
### Install dependency and compile:
```
 docker-compose run --rm npm install
 docker-compose run --rm npm run dev
```